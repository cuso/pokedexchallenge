import React, { useEffect, useState } from 'react';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Pagination from './Pagination';
import './PokeDetail.css';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

const PokeList = () => {
  const history = useHistory();
  const [listaPoke, setListaPoke] = useState(null);
  const [pokeFiltro, setPokeFiltro] = useState(null);
  const [count, setCount] = useState(0);
  const [filtroPok, setFiltroPok] = useState('');
  useEffect(() => {
    axios({
      method: 'GET',
      url: 'https://api.pokemontcg.io/v1/cards?subtype=Basic',
    })
      .then((response) => {
        setListaPoke(response.data.cards);
        setPokeFiltro(response.data.cards);
      });
  }, []);
  function decrement() {
    setFiltroPok('');
    if (count > 0) setCount(count - 1);
  }
  function increment() {
    setFiltroPok('');
    if (count * 8 + 7 < listaPoke.length) setCount(count + 1);
  }
  function filtro(e) {
    const text = e.target.value;
    setFiltroPok(text);
    const auxLista = listaPoke;
    if (text) {
      const pokeFiltrado = auxLista.filter((item) => {
        const itemData = item.name.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setPokeFiltro(pokeFiltrado);
    } else {
      setPokeFiltro(listaPoke);
    }
  }
  function dato(e) {
    history.push({
      pathname: '/detail',
      state: { detail: e },
    });
  }
  const classes = useStyles();
  return (
    <>
      <div style={{ padding: '10px 85px' }}>
        <div>
          <form className={classes.root} noValidate autoComplete="off">
            <TextField onChange={filtro} id="standard-basic" label="Filtro Pokemon" />
          </form>
        </div>
        <div style={{ paddingTop: '20px' }}>
          <Pagination increment={increment} decrement={decrement} count={count} />
        </div>
      </div>
      <Grid style={{ padding: '10px 75px' }} className="center" container>
        {pokeFiltro?.map((pokemon, index) => (
          (index >= count * 8 && index <= count * 8 + 7) || filtroPok
            ? (
              <Grid key={pokemon.id} item>
                <button style={{ margin: '10px' }} type="button" onClick={() => dato(pokemon)}>
                  <img src={pokemon.imageUrl} alt="imagen Pokemon" />
                  <br />
                  <span>
                    { pokemon.name}
                  </span>
                </button>
              </Grid>
            ) : (<></>)
        ))}
      </Grid>
    </>
  );
};
export default PokeList;
