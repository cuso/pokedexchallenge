import React from 'react';
import { Button } from '@material-ui/core';

// eslint-disable-next-line react/prop-types
const Pagination = ({ increment, decrement, count }) => (
  <>
    <Button onClick={decrement} variant="outlined" color="primary">Anterior</Button>
    <Button variant="contained" color="primary">
      {count}
    </Button>
    <Button onClick={increment} variant="outlined" color="primary">Siguiente</Button>
  </>
);

export default Pagination;
