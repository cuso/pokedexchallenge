import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import './PokeDetail.css';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import Chevron from '../image/Chevron.svg';

const PokeDetail = (props) => {
  const [pokemon, setPokemon] = useState(null);
  const history = useHistory();
  function back() {
    history.goBack();
  }
  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    setPokemon(props.history.location.state.detail);
  });
  return (
    <div>
      <section style={{ padding: '10px 85px' }}>
        <Button onClick={back} variant="outlined" color="primary">
          <img src={Chevron} alt="chevron" />
          <span style={{ marginLeft: '15px' }}>IR ATRAS</span>
        </Button>
      </section>
      <section>
        <h1 style={{ textAlign: 'center' }}>
          <span>{pokemon?.name}</span>
          &nbsp;
          <span className="pokemonNameText">N.º</span>
          <span className="pokemonNameText">{pokemon?.nationalPokedexNumber}</span>
        </h1>
      </section>
      <section className="center">
        <Grid style={{ maxWidth: '1024px' }} container spacing={3}>
          <Grid className="end" item xs={6}>
            <img src={pokemon?.imageUrl} alt="imagen Pokemon" />
          </Grid>
          <Grid item container xs={6}>
            <Grid style={{ backgroundColor: '#30a7d7', borderRadius: '10px', padding: '10px' }} item container xs={12}>
              {pokemon?.hp
                && (<PokeDetailElement name="Hit Point" value={pokemon?.hp} />)}
              {pokemon?.series
                && (<PokeDetailElement name="Series" value={pokemon?.series} />)}
              {pokemon?.set
                && (<PokeDetailElement name="Set" value={pokemon?.set} />)}
              {pokemon?.subtype
              && (<PokeDetailElement name="Subtype" value={pokemon?.subtype} />)}
            </Grid>
            {pokemon && (
            <>
              <Grid item container xs={12}>
                <Attacks pokemon={pokemon} />
              </Grid>
              <Grid item container xs={12}>
                <Resistances pokemon={pokemon} />
              </Grid>
            </>
            )}
          </Grid>
        </Grid>
      </section>
    </div>
  );
};

function Attacks(props) {
  const { pokemon } = props;
  return (
    <>
      {pokemon?.attacks
              && (
              <div className="column">
                <h3>Attacks</h3>
                <Grid container>
                  {pokemon?.attacks?.map((attack) => (
                    <Grid key={attack?.name} style={{ marginRight: '10px' }} item>
                      <span className="background-color-normal">{attack?.name}</span>
                    </Grid>
                  ))}
                </Grid>
              </div>
              )}
    </>
  );
}

Attacks.propTypes = {
  pokemon: PropTypes.arrayOf({
    name: PropTypes.string,
  }).isRequired,
};

function Resistances(props) {
  const { pokemon } = props;
  return (
    <>
      {pokemon?.resistances
              && (
              <div className="column">
                <h3>Resistances</h3>
                <Grid container>
                  {pokemon?.resistances?.map((resistance) => (
                    <Grid style={{ marginRight: '10px' }} key={resistance?.type}>
                      <span className="background-color-normal">{resistance?.type}</span>
                    </Grid>
                  ))}
                </Grid>
              </div>
              )}
    </>
  );
}

Resistances.propTypes = {
  pokemon: PropTypes.arrayOf({
    type: PropTypes.string.isRequired,
  }).isRequired,
};

function PokeDetailElement(props) {
  const { name, value } = props;
  return (
    <>
      {name && (
      <Grid item xs={6}>
        <div>
          <span style={{ color: '#fff' }}>{name}</span>
        </div>
        <div style={{ paddingTop: '0.5em' }}>
          <span style={{ color: '#212121' }}>{value}</span>
        </div>
      </Grid>
      )}
    </>
  );
}
PokeDetailElement.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};
export default PokeDetail;
