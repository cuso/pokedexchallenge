import './App.css';
import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import PokeList from './PokeModule/PokeList';
import PokeDetail from './PokeModule/PokeDetail';

function App() {
  return (
    <Router>
      <Route path="/" component={PokeList} exact />
      <Route path="/detail" component={PokeDetail} exact />
    </Router>
  );
}

export default App;
